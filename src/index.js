import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'antd/dist/antd.css';
import './index.css';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';
import {
  ROUTE_ROOT,
  ROUTE_PAGE_LOGIN
} from './routes';
import The404 from './screens/The404';
import Dashboard from './screens/Dashboard';
import Login from './screens/Login';

const { store } = configureStore();
ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path={ROUTE_ROOT} component={Dashboard} />  
        <Route exact path={ROUTE_PAGE_LOGIN} component={Login} />
        <Route component={The404} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
