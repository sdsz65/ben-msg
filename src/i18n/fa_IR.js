export const All = 'همه';
export const PAGE_404_TITLE = 'این صفحه وجود ندارد';
export const PAGE_404_ERROR_MESSAGE = 'صفحه پیدا نشد';
export const PAGE_404_BACK_LINK_TITLE = 'بازگشت به صفحه اصلی';