import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import appReducer from './App/Reducer';

const appPersistConfig = {
    key: 'app',
    storage,
    whitelist: [],
};
export default combineReducers({
    app: persistReducer(appPersistConfig, appReducer)
});