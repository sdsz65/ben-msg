import React, { Component } from 'react';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import styles from './index.module.css';

const { Header } = Layout;
class TheLayout extends Component {
    render() {
        return (
            <Layout>
                <Header className={styles.header}>

                </Header>
            </Layout>
        );
    };
};

const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});
export default connect(mapStateToProps, mapDispatchToProps)(TheLayout);
