import { 
    endpointURLFull,
    appBaseURLWithPort
} from 'config';
import axios from 'axios';
import { ROUTE_PAGE_LOGIN } from 'routes';


const API = axios.create({
  baseURL: endpointURLFull,
  timeout: 30000,
});

API.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export const GET = async (url, params = {}, config = {}) => {
  try {
    const data = await API.get(url, { params, ...config });
    checkPermission(data.data);
    return data.data;
  } catch (err) {
    return err;
  }
};

export const DELETE = async (url, body = {}) => {
  try {
    const data = await API.delete(url, { data: body });
    checkPermission(data.data);
    return data.data;
  } catch (err) {
    return err;
  }
};

const checkPermission = (data) => {
  if (!data.success && data.error) {
    window.location.href = `${appBaseURLWithPort}${ROUTE_PAGE_LOGIN}`;
  }
};
