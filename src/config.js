// ----------------------------------------------------------------------------------
// End Point Path Connection Config
// ----------------------------------------------------------------------------------
const endpointProtocol = 'https://';
const endpointDomain = 'reqres.in';
const endPointApi = 'api';
const endpointURL = `${endpointProtocol}${endpointDomain}`;
const endpointURLWithApi = `${endpointURL}/${endPointApi}`;
const endpointURLFull = `${endpointURLWithApi}/`;
// ----------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------
// App Path Connection Config
// ----------------------------------------------------------------------------------
const appBaseProtocol = 'http://';
const appBaseDomain = '127.0.0.1';
const appBasePort = '3002';
const appBaseURL = `${appBaseProtocol}${appBaseDomain}`;
const appBaseURLWithPort = `${appBaseURL}:${appBasePort}`;
const appBaseURLFull = `${appBaseURLWithPort}/`;
// ----------------------------------------------------------------------------------
const i18n = require('./i18n/en_US');
const antdConfigProviderI18n = require('antd/lib/locale/en_US');

module.exports = {
    // ----------------------------------------------------------------------------------
    // End Point Path Connection Config
    // ----------------------------------------------------------------------------------
    endpointProtocol,
    endpointDomain,
    endPointApi,
    endpointURL,
    endpointURLWithApi,
    endpointURLFull,
    // ----------------------------------------------------------------------------------
    // App Path Connection Config
    // ----------------------------------------------------------------------------------
    appBaseProtocol,
    appBaseDomain,
    appBasePort,
    appBaseURL,
    appBaseURLWithPort,
    appBaseURLFull,
    // ----------------------------------------------------------------------------------
  
    // ----------------------------------------------------------------------------------
    // App Config
    // ----------------------------------------------------------------------------------
    i18n,
    antdConfigProviderI18n
    // ----------------------------------------------------------------------------------
};