import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Result, Button } from 'antd';
import { appBaseURLFull, i18n } from 'config';

export default function index() {
  return (
    <div>
      <Helmet>
        <title>{i18n.PAGE_404_TITLE}</title>
        <link rel="canonical" href={`${appBaseURLFull}404`} />
      </Helmet>
      <Result
        status="404"
        title="404"
        subTitle={i18n.PAGE_404_ERROR_MESSAGE}
        extra={
          <Link to="/">
            <Button type="primary">{i18n.PAGE_404_BACK_LINK_TITLE}</Button>
          </Link>
        }
      />
    </div>
  );
}
