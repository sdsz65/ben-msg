import React, { Fragment } from 'react';
import TheLayout from 'components/UI/Layout';
import { Col, Row } from 'antd';

export default function Dashboard() {
  return (
    <Fragment>
      <TheLayout>
        <Row style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Col>...</Col>
        </Row>
      </TheLayout>
    </Fragment>
  );
}